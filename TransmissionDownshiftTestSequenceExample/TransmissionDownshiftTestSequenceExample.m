%% Test Downshift Points of a Transmission Controller
% This example demonstrates how to test a transmission shift logic
% controller using test sequences and test assessments.

%% The Model and Controller
% This example uses a simplified drivetrain system arranged in a controller-plant
% configuration. The objective of the example is to test the transmission
% controller in isolation, ensuring that it downshifts correctly.

%% The Test
% The controller should downshift between each of its gear ratios
% in response to a ramped throttle application.
% The test inputs hold vehicle speed constant while ramping the throttle.
% The Test Assessment block includes requirements-based
% assessments of the controller performance.
% path = fullfile(matlabroot,'examples','simulinktest');
path = 'D:\GIT\Matlab\Examples\simulinktest\TransmissionDownshiftTestSequenceExample'
mdl = 'TransmissionDownshiftTestSequence';
harness = 'TransmissionDownshiftTestSequence_Harness1';
% harness = 'controller_harness';
open_system(fullfile(path,mdl));

%% Open the Test Harness
% Click the badge on the subsystem |shift_controller| and open the test
% harness |controller_harness|. |shift_controller| is connected to a Test
% Sequence block and a Test Assessment block.

%%
sltest.harness.open([mdl '/shift_controller'],harness)

%% The Test Sequence
% Double-click the Test Sequence block to open the test sequence editor.
%
% The test sequence begins by ramping speed to 75 to initialize the 
% controller to fourth gear. Throttle is then ramped at constant speed 
% until a gear change. Subsequent initialization and downshifts execute. 
% After the change to first gear, the test sequence stops.

%%
% <<../DownshiftTestSequenceEditor.png>>

%% Test Assessments for the Controller
% 
% This example tests the following conditions:
%
% * Speed shall never be negative.
% * Gear shall always be positive.
% * Throttle shall be between 0% and 100%.
% * The shift controller shall keep the vehicle speed below specified
% maximums in each of the first three gears.

%%
%
% Open the Test Assessment block. The |assert| statements correspond
% to the first three conditions. If the controller
% violates one of the assertions, the simulation fails.
%
%%
%   assert(speed >= 0, 'speed must be >= 0');
%   assert(throttle >= 0, 'throttle must be >= 0 and <= 100');
%   assert(throttle <= 100, 'throttle must be >= 0 and <= 100');
%   assert(gear > 0,'gear must be > 0');
%
%%
% The last condition is checked by three individual |verify| statements
% corresponding to the maximum speed in gears 1, 2, and 3:
%
% * The controller shall not let the vehicle speed exceed 90 in gear 3.
% * The controller shall not let the vehicle speed exceed 50 in gear 2.
% * The controller shall not let the vehicle speed exceed 30 in gear 1.
%
% The |verify| statements are contained in a When decomposition sequence.
% The active When decomposition step is determined by 
% on signal conditions defined in the *Step* column, with each condition 
% preceded by the |when| operator. The last step |Else| 
% covers any undefined condition and does not use a |when|
% declaration. For more information on When decomposition, see
% <https://www.mathworks.com/help/sltest/ug/test-sequence-editor.html Test Sequence Editor>.
% 
% 
%%
%
%   OverSpeed3 when gear==3
%   verify(speed <= 90,'Engine overspeed in gear 3')
%
%   OverSpeed2 when gear==2
%   verify(speed <= 50,'Engine overspeed in gear 2')
%  
%   OverSpeed1 when gear==1
%   verify(speed <= 30,'Engine overspeed in gear 1')
%%
% <<../DownshiftAssessmentBlockWithWhenDecomp.png>>

%% Testing the Controller
%
% Simulating the test harness demonstrates the progressive throttle ramp at 
% each test step and the corresponding downshifts. The controller passes
% all of the assessments in the Test Assessment block.
%%
% open_system([harness '/FloatingScope'])
open_system([harness '/Scope'])
sim(harness);

%% View the Results
%
% Click the Simulation Data Inspector button in the test harness toolstrip
% to view the results. You can compare the |speed| signal to the |verify|
% statement outputs.

Simulink.sdi.view

%%
%
% <<../sdi_visualization.png>>

%%
Simulink.sdi.close
close_system(mdl);

